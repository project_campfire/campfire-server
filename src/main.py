#!/bin/python3
#TODO: Move possible configs to configuration file
# CONFIGURATION OPTIONS:
        #[fullchain, privkey(optional)]
SSLPATH=[]


import websockets, asyncio, json, os, flask, flask_cors
from threading import Thread
import ssl
from random import randint
from base64 import b64encode
#                      uID: [socket]
authenticated_users = {             }
campsites = {}
users = {}
admins = []
msglog = {}
def save():
    print("Saving Users...") 
    # Save users
    for user in list( users.keys() ):
        del users[user]["picture"]
        user = hex(user)
        if not os.path.exists("users/" + str(user)):
            os.system(f"mkdir -p users/{user} && touch 'users/{user}/info.json'")
        with open(f"users/{user}/info.json", "w") as userfl:
            userfl.write(json.dumps(users[int(user, 16)]))
    print("Saving message log")
    for msglist in msglog:
        try:
            with open(f"campsites/{msglist}/messages.json", "w") as msgfl:
                msgfl.write(json.dumps(msglog[msglist]))
        except Exception as err: #TODO: Implement DM message saving(?)
            print("Unable to read messages.json")
            print("Python error:", err)
def load_users():
    global users, admins
    users = {}
    admins = []
    for user in os.listdir("users"):
        user = int(user, 16)
        users[user] = {}
        hexuser = hex(user)
        with open(f"users/{hexuser}/info.json") as usersfl:
            print(user)
            details = json.loads(usersfl.read())
            users[user] = details
            print(f"users/{hexuser}/picture.jpg")
            if "admin" in details and details["admin"]:
                admins.append(user)
        users[user]["picture"] = f"users/{hexuser}/picture.jpg"
def load_campsites():
    global campsites, msglog
    campsites = {}
    for campsite in os.listdir("campsites"):
        campsites[campsite] = {}
        with open(f"campsites/{campsite}/info.json") as campsitefl:
            details = json.loads(campsitefl.read())
            print(details["users"])
            users = []
            for user in details["users"]:
                user = int(user, 16)
                print(user)
                users.append(user)
            details["users"] = users 
            print(details)
            campsites[campsite] = details
        with open("campsites/" + campsite + "/messages.json") as msgfl:
            msglog[campsite] = json.loads(msgfl.read())[-50:]

def uid_from_username(username):
    global users
    for user in list( users.keys() ):
        if users[user]["username"] == username:
            return user
    return False
if not os.path.exists("users"):
    print("Creating users directory...")
    os.makedirs("users")
print("loading users...")
load_users()
print("Loading campsites...")
load_campsites()
async def handler(ws, path):
    global valid_keys, authenticated_users, campsites
    initmsg = await ws.recv()
    if initmsg == "campfireping":
        await ws.send("true")
        await ws.close()
    try:
        msg = json.loads(initmsg)
        username = msg["username"]
        password = msg["passhash"]
    except Exception as err:
        print("Error: Client sent invalid json. closing socket")
        print("Python error:", err)
        ws.close()
        return
    if isinstance(uid_from_username(username), bool):
        print(f"User tried to login with username '{username}' but was not recognized. closing connection.")
        await ws.send(json.dumps({"Error": "Invalid Credintials"}))
        await ws.close()
        return
    currentUid = uid_from_username(username)
    if password != users[currentUid]["passhash"] and users[currentUid]["passhash"] != None:
        print(f"User tried to login with wrong password.")
        await ws.send(json.dumps({"Error": "Invalid Credintials"}))
        await ws.close()
        return
    elif "create" in msg:
        await ws.send(json.dumps({"Error": "Not Implemented"}))
    admin = currentUid in admins
    print(admin)
    print(f"User '{username}' has been authorized")
    closed = asyncio.ensure_future(ws.wait_closed())
    closed.add_done_callback(lambda remove: removeuser(username, ws)) #())
    await ws.send("OK")
    if currentUid in authenticated_users:
        print("User not already in")
        authenticated_users[currentUid].append(ws)
    else:
        print("user not in")
        authenticated_users[currentUid] = [ws]
    async for msg in ws: # Message loop
        try: # more error checking...
            msg = json.loads(msg)
            #Checking if values exist in message
            msgdata = msg["message"]
            msgtype = msg["type"]
            recipiant = msg["recipiant"]
        except:
            print(f"User '{username}' sent a message with invalid json.")
            if len(msg) <= 100: #Log if message size less than 100 bytes
                print("Message:", msgdata)
            else:
                print("Message not printed due to being over the 100 byte log limit")
            await ws.send(json.dumps({"Error": "Message was not sent in JSON or data that was expected was not in message."}))
            continue
        # Error checking finally done :)
        # Now checking if its a command
        if not msgtype == "text" or not msgtype == "message" and type(msgdata) == list:
            msgdata = b64encode(bytes(msgdata)).decode()
        if "commandargs" in msg and type(msg["commandargs"]) == list: # is a command.
            if msgdata == "LISTUSERS":
                site = msg["commandargs"][0]
                userlist = []
                for user in campsites[site]["users"]:
                    userlist.append(users[user]["username"])
                await ws.send(json.dumps({"users": userlist, "type": "SYSTEMMESSAGE"}))
                continue
            if msgdata == "LISTCAMPSITES":
                sitelist = []
                print("SENDING CAMPSITES")
                for campsite in list( campsites.keys() ):
                    if currentUid in campsites[campsite]["users"]:
                        sitelist.append(campsite)
                await ws.send(json.dumps( {"campsites": sitelist, "type": "SYSTEMMESSAGE"} ))
            if msgdata == "PREVIOUSMSG":
                try:
                    area = msg["commandargs"][0]
                    amount = msg["commandargs"][1]
                except:
                    print("Arguments are unavailable")
                    continue
                await ws.send(json.dumps(get_messages(area, amount)))
            if msgdata == "CREATEUSER" and admin:
                try:
                    username = msg["commandargs"][0]
                    passhash = msg["commandargs"][1]
                except:
                    print("Arguments are unavailable")
                    continue
                users[hex(randint(int("0x10000000", 16), int("0xffffffff", 16)))] = {"username": username, "passhash": passhash}
                print("Created user")
            if msgdata == "LISTONLINEUSERS":
                try:
                    campsite = msg["commandargs"][0]
                except:
                    print("Arguments are unavailable")
                    continue
                try:
                    sendPicture = msg["commandargs"][1]
                except:
                    sendPicture = None
                userlist = []
                pictures = {}
                for user in list ( authenticated_users.keys() ):
                    if users[user]["username"] in userlist:
                        continue
                    print(user)
                    if user in campsites[campsite]["users"]:
                        userlist.append(users[user]["username"])
                    if sendPicture:
                        pictures[users[user]["username"]] = str(users[user]["picture"])
                print(userlist)
                await ws.send(json.dumps( {"onlineusers": userlist, "type": "SYSTEMMESSAGE", "pictures": pictures} ))
                continue
            if msgdata == "SAVE" and admin:
                save()
            if msgdata == "SETTINGS":
                try:
                    settings = msg["commandargs"][0]
                except:
                    print("Arguments are unavailable")
                    continue
                if includesOther(list( settings.keys() ), ["username", "passhash", "picture"]) and not username in admins:
                    print("Invalid detail included, canceling...")
                    await ws.send(json.dumps( {"Error": "Invalid detail(s) sent"} ))
                    continue
                if "picture" in settings:
                    try:
                        with open(f"/tmp/{hex(currentUid)}_picture.jpg", "wb") as picturefl:
                            picturefl.write(bytes(settings["picture"]))
                        os.system(f"ffmpeg -i /tmp/{hex(currentUid)}_picture.jpg -y -vf scale=128:128 users/{hex(currentUid)}/picture.jpg && rm /tmp/{hex(currentUid)}_picture.jpg")
                        continue
                    except Exception as err:
                        print(f"Unable to write to file\n{err}")
                users[currentUid] = combinedict(settings, users[currentUid])
                print(users)
                save()
            continue

        # Blast the data out to everyone it needs to go to!
        sendmsg = {"sender": username, "message": msgdata, "type": msgtype, "campsite": recipiant}
        msglog[recipiant].append(sendmsg)
        if recipiant in campsites:
            for user in campsites[recipiant]["users"]:
                if not user in authenticated_users:
                    continue
                for socket in authenticated_users[user]:
                    await socket.send(json.dumps(sendmsg))
            continue
        user = uid_from_username(recipiant)
        if not isinstance(user, bool):
            try:
                for socket in authenticated_users[uid_from_username(recipiant)]:
                    print(socket)
                    await socket.send(json.dumps({"sender": username, "message": msgdata, "type": msgtype, "recipiant": recipiant}))
                for socket in authenticated_users[uid_from_username(username)]:
                    await ws.send(json.dumps({"sender": username, "message": msgdata, "type": msgtype, "recipiant": recipiant}))
            except:
                await ws.send(json.dumps({"Error": "Message not sent: User is not online"}))
        else:
            await ws.send(json.dumps({"Error": "Recipiant does not exist"}))
            continue
def get_messages(area, amount):
    global campsites
    if not area in campsites and not uid_from_username(area):
        return {"Error": "Recipiant does not exist"}
    if area in campsites:
        return msglog[area][-amount:]
        #TODO
def removeuser(username, ws):
    print(authenticated_users)
    uid = uid_from_username(username)
    authenticated_users[uid].remove(ws)
    if len(authenticated_users[uid]) == 0:
        del authenticated_users[uid]
#   Returns False if all items from originallist are in whitelist.
def includesOther(originallist, whitelist):
    for item in originallist:
        if not item in whitelist:
            return True
    return False
def combinedict(primdict, secodict):
    for item in list( primdict.keys() ):
        secodict[item] = primdict[item]
    return secodict


def webserver():
    global users, SSLPATH
    webserver = flask.Flask(__name__)
    flask_cors.CORS(webserver)
    @webserver.route("/picture")
    def picture():
        user = uid_from_username(flask.request.args.get("user"))
        print(users[user])
        return flask.send_file(users[user]["picture"], mimetype="image/jpg")
    if len(SSLPATH) != 0:
        webserver.run(host="0.0.0.0", ssl_context=(SSLPATH[0], SSLPATH[1]))
    else:
        webserver.run(host="0.0.0.0", ssl_context=(SSLPATH[0], SSLPATH[1]))


try:
    thread = Thread(target=webserver)
    thread.daemon = True
    thread.start()
    # Load SSL
    if len(SSLPATH) != 0:
        sslc = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
        sslc.load_cert_chain(SSLPATH[0], SSLPATH[1])

        server = websockets.serve(handler, port=5001, max_size=30*1024*1024, ssl=sslc)
    else:
        server = websockets.serve(handler, port=5001, max_size=30*1024*1024)
    print("Server running")

    asyncio.get_event_loop().run_until_complete(server)
    asyncio.get_event_loop().run_forever()
except KeyboardInterrupt:
    save()
    exit(0)
